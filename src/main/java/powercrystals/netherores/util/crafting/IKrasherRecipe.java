package powercrystals.netherores.util.crafting;

import net.minecraft.item.ItemStack;

public interface IKrasherRecipe {
    ItemStack getInput();

    ItemStack getOutput();

    int getEnergy();

    boolean isOutputFood();
}

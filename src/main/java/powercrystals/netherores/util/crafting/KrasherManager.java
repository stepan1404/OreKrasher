package powercrystals.netherores.util.crafting;

import cofh.core.util.oredict.OreDictionaryArbiter;
import cofh.lib.inventory.ComparableItemStack;
import cofh.lib.util.helpers.ItemHelper;
import cofh.thermalexpansion.ThermalExpansion;
import cofh.thermalfoundation.item.TFItems;
import gnu.trove.map.hash.THashMap;
import gnu.trove.set.hash.THashSet;
import net.minecraft.block.Block;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemFood;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.FurnaceRecipes;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

public class KrasherManager {

    private static Map<ComparableItemStackKrasher, KrasherManager.RecipeKrasher> recipeMap = new THashMap();
    private static boolean allowOverwrite = false;
    public static final int DEFAULT_ENERGY = 1600;

    public KrasherManager() {
    }

    public static KrasherManager.RecipeKrasher getRecipe(ItemStack var0) {
        if (var0 == null) {
            return null;
        } else {
            ComparableItemStackKrasher var1 = new ComparableItemStackKrasher(var0);
            KrasherManager.RecipeKrasher var2 = (KrasherManager.RecipeKrasher)recipeMap.get(var1);
            if (var2 == null) {
                var1.metadata = 32767;
                var2 = (KrasherManager.RecipeKrasher)recipeMap.get(var1);
            }

            return var2;
        }
    }

    public static boolean recipeExists(ItemStack var0) {
        return getRecipe(var0) != null;
    }

    public static KrasherManager.RecipeKrasher[] getRecipeList() {
        return (KrasherManager.RecipeKrasher[])recipeMap.values().toArray(new KrasherManager.RecipeKrasher[0]);
    }

    public static void refreshRecipes() {
        THashMap var0 = new THashMap(recipeMap.size());
        THashSet var1 = new THashSet();
        Iterator var3 = recipeMap.entrySet().iterator();

        while(var3.hasNext()) {
            Map.Entry var4 = (Map.Entry)var3.next();
            KrasherManager.RecipeKrasher var2 = (KrasherManager.RecipeKrasher)var4.getValue();
            var0.put(new ComparableItemStackKrasher(var2.input), var2);
            if (var2.isOutputFood()) {
                var1.add(new ComparableItemStackKrasher(var2.input));
            }
        }

        recipeMap.clear();
        recipeMap = var0;
    }

    public static boolean addTERecipe(int energy, ItemStack input, ItemStack output) {
        if (input != null && output != null && energy > 0) {
            KrasherManager.RecipeKrasher recipe = new KrasherManager.RecipeKrasher(input, output, energy);
            recipeMap.put(new ComparableItemStackKrasher(input), recipe);
            return true;
        } else {
            return false;
        }
    }

    public static boolean addRecipe(int energy, ItemStack input, ItemStack output, boolean var3) {
        if (input != null && output != null && energy > 0 && (allowOverwrite & var3 || recipeMap.get(new ComparableItemStackKrasher(input)) == null)) {
            KrasherManager.RecipeKrasher var4 = new KrasherManager.RecipeKrasher(input, output, energy);
            recipeMap.put(new ComparableItemStackKrasher(input), var4);
            return true;
        } else {
            return false;
        }
    }

    public static boolean removeRecipe(ItemStack var0) {
        return recipeMap.remove(new ComparableItemStackKrasher(var0)) != null;
    }

    public static void addOreDictRecipe(String ore, ItemStack output) {
        addOreDictRecipe(1600, ore, output);
    }

    public static void addOreDictRecipe(int energy, String ore, ItemStack output) {
        if (ItemHelper.oreNameExists(ore)) {
            addRecipe(energy, ItemHelper.cloneStack((ItemStack)OreDictionaryArbiter.getOres(ore).get(0), 1), output, false);
        }

    }

    static {
        allowOverwrite = ThermalExpansion.config.get("RecipeManagers.Krasher", "AllowRecipeOverwrite", false);
    }

    public static class ComparableItemStackKrasher extends ComparableItemStack {
        static final String ORE = "ore";
        static final String DUST = "dust";

        public static boolean safeOreType(String var0) {
            return var0.startsWith("ore") || var0.startsWith("dust");
        }

        public static int getOreID(ItemStack var0) {
            ArrayList var1 = OreDictionaryArbiter.getAllOreIDs(var0);
            if (var1 != null) {
                int var2 = 0;
                int var3 = var1.size();

                while(var2 < var3) {
                    int var4 = (Integer)var1.get(var2++);
                    if (var4 != -1 && safeOreType(ItemHelper.oreProxy.getOreName(var4))) {
                        return var4;
                    }
                }
            }

            return -1;
        }

        public ComparableItemStackKrasher(ItemStack var1) {
            super(var1);
            this.oreID = getOreID(var1);
        }

        public ComparableItemStackKrasher(Item var1, int var2, int var3) {
            super(var1, var2, var3);
            this.oreID = getOreID(this.toItemStack());
        }

        public ComparableItemStackKrasher set(ItemStack var1) {
            super.set(var1);
            this.oreID = getOreID(var1);
            return this;
        }
    }

    public static class RecipeKrasher implements IKrasherRecipe {
        final ItemStack input;
        final ItemStack output;
        final int energy;
        boolean isOutputFood;

        RecipeKrasher(ItemStack var1, ItemStack var2, int var3) {
            this.input = var1;
            this.output = var2;
            this.energy = var3;
            if (var1.stackSize <= 0) {
                var1.stackSize = 1;
            }

            if (var2.stackSize <= 0) {
                var2.stackSize = 1;
            }

            if (var2.getItem() instanceof ItemFood) {
                this.isOutputFood = true;
            }

        }

        public boolean isOutputFood() {
            return this.isOutputFood;
        }

        public ItemStack getInput() {
            return this.input.copy();
        }

        public ItemStack getOutput() {
            return this.output.copy();
        }

        public int getEnergy() {
            return this.energy;
        }
    }

}

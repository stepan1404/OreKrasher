package powercrystals.netherores.block.machine;

import cofh.core.util.CoreUtils;
import cofh.lib.util.helpers.MathHelper;
import cofh.thermalexpansion.ThermalExpansion;
import cofh.thermalexpansion.block.machine.BlockMachine;
import cofh.thermalexpansion.block.machine.BlockMachine.Types;
import cofh.thermalexpansion.block.machine.ItemBlockMachine;
import cofh.thermalexpansion.block.machine.TileMachineBase;
import cpw.mods.fml.common.registry.GameRegistry;
import net.minecraft.block.Block;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import powercrystals.netherores.gui.client.machine.GuiKrasher;
import powercrystals.netherores.gui.container.machine.ContainerKrasher;
import powercrystals.netherores.util.crafting.KrasherManager;

public class TileKrasher extends TileMachineBase {

    int inputTracker;
    int outputTracker;

    public static void init(Block block){
        TileKrasher.initialize();
        BlockMachine.krasher = ItemBlockMachine.setDefaultTag(new ItemStack(block, 1, BlockMachine.Types.KRASHER.ordinal()));
        GameRegistry.registerCustomItemStack("krasher", BlockMachine.krasher);
    }

    public static void initialize() {
        int var0 = Types.KRASHER.ordinal();
        defaultSideConfig[var0] = new SideConfig();
        defaultSideConfig[var0].numConfig = 4;
        defaultSideConfig[var0].slotGroups = new int[][]{new int[0], {0}, {1}, {0, 1}};
        defaultSideConfig[var0].allowInsertionSide = new boolean[]{false, true, false, true};
        defaultSideConfig[var0].allowExtractionSide = new boolean[]{false, true, true, true};
        defaultSideConfig[var0].allowInsertionSlot = new boolean[]{true, false, false};
        defaultSideConfig[var0].allowExtractionSlot = new boolean[]{true, true, false};
        defaultSideConfig[var0].sideTex = new int[]{0, 1, 4, 7};
        defaultSideConfig[var0].defaultSides = new byte[]{1, 1, 2, 2, 2, 2};
        String var1 = "Machine.Krasher";
        int var2 = MathHelper.clamp(ThermalExpansion.config.get(var1, "BasePower", 20), 10, 500);
        ThermalExpansion.config.set(var1, "BasePower", var2);
        defaultEnergyConfig[var0] = new EnergyConfig();
        defaultEnergyConfig[var0].setParamsPower(var2);
        sounds[var0] = CoreUtils.getSoundName("ThermalExpansion", "blockMachineFurnace");
        GameRegistry.registerTileEntity(TileKrasher.class, "orekrasher.Krasher");
    }

    public TileKrasher() {
        super(Types.KRASHER);
        this.inventory = new ItemStack[3];
    }

    protected boolean canStart() {
        if (this.inventory[0] == null) {
            return false;
        } else {
            KrasherManager.RecipeKrasher var1 = KrasherManager.getRecipe(this.inventory[0]);
            if (var1 != null && this.energyStorage.getEnergyStored() >= var1.getEnergy() * this.energyMod / this.processMod) {
                ItemStack var2 = var1.getOutput();
                if (this.inventory[1] == null) {
                    return true;
                } else if (!this.inventory[1].isItemEqual(var2)) {
                    return false;
                } else {
                    return this.inventory[1].stackSize + var2.stackSize <= var2.getMaxStackSize();
                }
            } else {
                return false;
            }
        }
    }

    protected boolean hasValidInput() {
        KrasherManager.RecipeKrasher var1 = KrasherManager.getRecipe(this.inventory[0]);
        return var1 == null ? false : var1.getInput().stackSize <= this.inventory[0].stackSize;
    }

    protected void processStart() {
        this.processMax = KrasherManager.getRecipe(this.inventory[0]).getEnergy();

        this.processRem = this.processMax;
    }

    protected void processFinish() {
        KrasherManager.RecipeKrasher var1 = KrasherManager.getRecipe(this.inventory[0]);
        if (var1 == null) {
            this.isActive = false;
            this.wasActive = true;
            this.tracker.markTime(this.worldObj);
            this.processRem = 0;
        } else {
            ItemStack var2 = var1.getOutput();
            if (this.inventory[1] == null) {
                this.inventory[1] = var2;
            } else {
                this.inventory[1].stackSize += var2.stackSize;
            }

            this.inventory[0].stackSize -= var1.getInput().stackSize;

            if (this.inventory[0].stackSize <= 0) {
                this.inventory[0] = null;
            }

        }
    }

    protected void transferInput() {
        if (this.augmentAutoInput) {
            for(int var2 = this.inputTracker + 1; var2 <= this.inputTracker + 6; ++var2) {
                int var1 = var2 % 6;
                if (this.sideCache[var1] == 1 && this.extractItem(0, AUTO_TRANSFER[this.level], var1)) {
                    this.inputTracker = var1;
                    break;
                }
            }

        }
    }

    protected void transferOutput() {
        if (this.augmentAutoOutput) {
            if (this.inventory[1] != null) {
                for(int var2 = this.outputTracker + 1; var2 <= this.outputTracker + 6; ++var2) {
                    int var1 = var2 % 6;
                    if (this.sideCache[var1] == 2 && this.transferItem(1, AUTO_TRANSFER[this.level], var1)) {
                        this.outputTracker = var1;
                        break;
                    }
                }

            }
        }
    }

    public Object getGuiClient(InventoryPlayer var1) {
        return new GuiKrasher(var1, this);
    }

    public Object getGuiServer(InventoryPlayer var1) {
        return new ContainerKrasher(var1, this);
    }

    public void readFromNBT(NBTTagCompound var1) {
        super.readFromNBT(var1);
        this.inputTracker = var1.getInteger("TrackIn");
        this.outputTracker = var1.getInteger("TrackOut");
    }

    public void writeToNBT(NBTTagCompound var1) {
        super.writeToNBT(var1);
        var1.setInteger("TrackIn", this.inputTracker);
        var1.setInteger("TrackOut", this.outputTracker);
    }

    protected boolean installAugment(int var1) {
        return super.installAugment(var1);
    }

    protected void resetAugments() {
        super.resetAugments();
    }

    public boolean isItemValidForSlot(int var1, ItemStack var2) {
        return var1 == 0 ? (KrasherManager.recipeExists(var2)) : true;
    }
}

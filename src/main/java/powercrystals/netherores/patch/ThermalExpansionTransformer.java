package powercrystals.netherores.patch;

import net.minecraft.client.Minecraft;
import net.minecraft.launchwrapper.IClassTransformer;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.objectweb.asm.ClassReader;
import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.Opcodes;
import org.objectweb.asm.tree.*;

import java.util.HashMap;
import java.util.Map;

public class ThermalExpansionTransformer implements IClassTransformer, Opcodes {

    private static final Map<String, String> mapper = new HashMap<>();

    @Override
    public byte[] transform(String name, String transformedName, byte[] basicClass) {
        try {
            if (name.equals("cofh.thermalexpansion.block.machine.BlockMachine$Types")) {
                LogManager.getLogger("LetsPlay-OreKrasher").log(Level.INFO, "Founded " + transformedName);
                return this.transformTypes(basicClass);
            }

            if (name.equals("cofh.thermalexpansion.block.machine.BlockMachine")) {
                LogManager.getLogger("LetsPlay-OreKrasher").log(Level.INFO, "Founded " + transformedName);
                return this.transformMachine(basicClass);
            }

            if (name.equals("cofh.thermalexpansion.block.machine.TileMachineBase")){
                LogManager.getLogger("LetsPlay-OreKrasher").log(Level.INFO, "Founded " + transformedName);
                return this.transformTileMachine(basicClass);
            }

        } catch (Throwable t) {
            LogManager.getLogger("LetsPlay-OreKrasher").log(Level.ERROR, "An issue occoured while transforming " + transformedName);
            t.printStackTrace();
        }
        return basicClass;
    }

    private byte[] transformTileMachine(byte[] basicClass) {
        ClassNode classNode = new ClassNode();
        ClassReader classReader = new ClassReader(basicClass);

        classReader.accept(classNode, 0);

        for(FieldNode fieldNode : classNode.fields){
            if(fieldNode.access == 0){
                fieldNode.access = ACC_PUBLIC;
                LogManager.getLogger("LetsPlay-OreKrasher").log(Level.INFO, "Field: " + fieldNode.name);
            }
        }

        for(MethodNode m : classNode.methods){
            if(!m.name.equals("<clinit>")){
                continue;
            }

            for(int i = 0; i < m.instructions.size(); i++){
                AbstractInsnNode abstractInsnNode = m.instructions.get(i);

                if(abstractInsnNode instanceof IntInsnNode){
                    IntInsnNode intInsnNode = (IntInsnNode) abstractInsnNode;

                    if(intInsnNode.getOpcode() == BIPUSH && intInsnNode.operand == 12){
                        m.instructions.insertBefore(intInsnNode, new IntInsnNode(BIPUSH, 13));
                        m.instructions.remove(intInsnNode);
                        break;
                    }
                }
            }

            for(int i = 0; i < m.instructions.size(); i++){
                AbstractInsnNode abstractInsnNode = m.instructions.get(i);

                if(abstractInsnNode instanceof IntInsnNode){
                    IntInsnNode intInsnNode = (IntInsnNode) abstractInsnNode;

                    if(intInsnNode.getOpcode() == BIPUSH && intInsnNode.operand == 15){
                        AbstractInsnNode prevNode = intInsnNode.getPrevious();

                        if(prevNode instanceof IntInsnNode){
                            IntInsnNode intInsnNode1 = (IntInsnNode) prevNode;

                            if(intInsnNode1.operand == 11){
                                InsnList insnList = new InsnList();

                                insnList.add(new InsnNode(DUP));
                                insnList.add(new IntInsnNode(BIPUSH, 12));
                                insnList.add(new IntInsnNode(BIPUSH, 4));
                                insnList.add(new InsnNode(IASTORE));

                                m.instructions.insert(intInsnNode.getNext(), insnList);
                                break;
                            }
                        }
                    }
                }
            }

        }

        LogManager.getLogger("LetsPlay-OreKrasher").log(Level.INFO, "Transformed TileMachineBase.");

        ClassWriter classWriter = new ClassWriter(3);
        classNode.accept(classWriter);
        return classWriter.toByteArray();
    }

    private byte[] transformMachine(byte[] basicClass) {
        ClassNode classNode = new ClassNode();
        ClassReader reader = new ClassReader(basicClass);

        reader.accept(classNode, ClassReader.EXPAND_FRAMES);

        classNode.fields.add(new FieldNode(ACC_PUBLIC + ACC_STATIC, "krasher", "Lnet/minecraft/item/ItemStack;", null, null));

        for (MethodNode m : classNode.methods) {
            if(!m.name.equals("<clinit>"))
                continue;

            for(int i = 0; i < m.instructions.size(); i++){
                AbstractInsnNode abstractInsnNode = m.instructions.get(i);

                if(abstractInsnNode instanceof IntInsnNode){
                    IntInsnNode intInsnNode = (IntInsnNode) abstractInsnNode;
                    if(intInsnNode.getOpcode() == BIPUSH && intInsnNode.operand == 12){
                        m.instructions.insertBefore(intInsnNode, new IntInsnNode(BIPUSH, 13));
                        m.instructions.remove(intInsnNode);
                        break;
                    }
                }
            }

            for(int i = 0; i < m.instructions.size(); i++){
                AbstractInsnNode abstractInsnNode = m.instructions.get(i);

                if(abstractInsnNode instanceof LdcInsnNode){
                    LdcInsnNode ldcInsnNode = (LdcInsnNode) abstractInsnNode;

                    if(ldcInsnNode.cst.equals("insolator")){
                        InsnList insnList = new InsnList();
                        insnList.add(new InsnNode(DUP));
                        insnList.add(new IntInsnNode(BIPUSH, 12));
                        insnList.add(new LdcInsnNode("krasher"));
                        insnList.add(new InsnNode(AASTORE));

                        m.instructions.insert(abstractInsnNode.getNext(), insnList);
                        break;
                    }
                }
            }
        }

        for(MethodNode m : classNode.methods){
            if(!m.name.equals("initialize")){
                continue;
            }

            for(int i = m.instructions.size() - 1; i >= 0 ; i--) {
                AbstractInsnNode abstractInsnNode = m.instructions.get(i);

                if(abstractInsnNode.getOpcode() == ICONST_1){
                    InsnList insnList = new InsnList();

                    insnList.add(new VarInsnNode(ALOAD, 0));
                    insnList.add(new MethodInsnNode(INVOKESTATIC, "powercrystals/netherores/block/machine/TileKrasher", "init", "(Lnet/minecraft/block/Block;)V", false));

                    /*LabelNode l43 = new LabelNode();
                    insnList.add(l43);
                    insnList.add(new LineNumberNode(289, l43));
                    insnList.add(new FrameNode(F_SAME, 0, null, 0, null));
                    insnList.add(new MethodInsnNode(INVOKESTATIC, "powercrystals/netherores/block/machine/TileKrasher", "initialize", "()V", false));

                    LabelNode l44 = new LabelNode();
                    insnList.add(l44);
                    insnList.add(new LineNumberNode(290, l44));
                    insnList.add(new TypeInsnNode(NEW, "net/minecraft/item/ItemStack"));
                    insnList.add(new InsnNode(DUP));
                    insnList.add(new VarInsnNode(ALOAD, 0));
                    insnList.add(new InsnNode(ICONST_1));
                    insnList.add(new FieldInsnNode(GETSTATIC, "cofh/thermalexpansion/block/machine/BlockMachine$Types", "KRASHER", "Lcofh/thermalexpansion/block/machine/BlockMachine$Types;"));
                    insnList.add(new MethodInsnNode(INVOKEVIRTUAL, "cofh/thermalexpansion/block/machine/BlockMachine$Types", "ordinal", "()I", false));
                    insnList.add(new MethodInsnNode(INVOKESPECIAL, "net/minecraft/item/ItemStack", "<init>", "(Lnet/minecraft/block/Block;II)V", false));
                    insnList.add(new MethodInsnNode(INVOKEVIRTUAL, "cofh/thermalexpansion/block/machine/ItemBlockMachine", "setDefaultTag", "(Lnet/minecraft/item/ItemStack;)Lnet/minecraft/item/ItemStack;", false));
                    insnList.add(new FieldInsnNode(PUTSTATIC, "cofh/thermalexpansion/block/machine/BlockMachine", "krasher", "Lnet/minecraft/item/ItemStack;"));

                    LabelNode l45 = new LabelNode();
                    insnList.add(l45);
                    insnList.add(new LineNumberNode(291, l45));
                    insnList.add(new LdcInsnNode("krasher"));
                    insnList.add(new FieldInsnNode(GETSTATIC, "cofh/thermalexpansion/block/machine/BlockMachine", "krasher", "Lnet/minecraft/item/ItemStack;"));
                    insnList.add(new MethodInsnNode(INVOKESTATIC, "cpw/mods/fml/common/registry/GameRegistry", "registerCustomItemStack", "(Ljava/lang/String;Lnet/minecraft/item/ItemStack;)V", false));

                    LabelNode l46 = new LabelNode();
                    insnList.add(l46);
                    insnList.add(new LineNumberNode(293, l46));
                    insnList.add(new InsnNode(ICONST_1));
                    insnList.add(new InsnNode(IRETURN));

                    AbstractInsnNode node = abstractInsnNode.getPrevious().getPrevious().getPrevious();

                    m.instructions.remove(abstractInsnNode.getPrevious().getPrevious());
                    m.instructions.remove(abstractInsnNode.getPrevious());
                    m.instructions.remove(abstractInsnNode);*/
                    m.instructions.insertBefore(abstractInsnNode, insnList);
                    break;
                } /*else {
                    m.instructions.remove(abstractInsnNode);
                }*/
            }

            /*for(int i = 0; i < m.instructions.size(); i++) {
                AbstractInsnNode abstractInsnNode = m.instructions.get(i);

                LogManager.getLogger("LetsPlay-OreKrasher").log(Level.INFO, String.format("Opcode: %d", abstractInsnNode.getOpcode()));
            }*/
        }

        for(MethodNode m : classNode.methods){
            if(!m.name.equals("createNewTileEntity") && !m.name.equals("func_149915_a")){
                continue;
            }

            LabelNode l40 = new LabelNode();
            FrameNode frameNode = new FrameNode(F_NEW, 3, new Object[]{"Lcofh/thermalexpansion/block/machine/BlockMachine;", "Lnet/minecraft/world/World;", INTEGER}, 0, new Object[]{});
            InsnList insnList = new InsnList();
            insnList.add(l40);
            insnList.add(frameNode);
            insnList.add(new TypeInsnNode(NEW, "powercrystals/netherores/block/machine/TileKrasher"));
            insnList.add(new InsnNode(DUP));
            insnList.add(new MethodInsnNode(INVOKESPECIAL, "powercrystals/netherores/block/machine/TileKrasher", "<init>", "()V", false));
            insnList.add(new InsnNode(ARETURN));

            m.instructions.add(insnList);

            for(int i = 0; i < m.instructions.size(); i++) {
                AbstractInsnNode abstractInsnNode = m.instructions.get(i);

                if(abstractInsnNode.getOpcode() == ILOAD){
                    if(((VarInsnNode) abstractInsnNode).var == 2){
                        InsnList insnList2 = new InsnList();

                        insnList2.add(new VarInsnNode(ILOAD, 2));
                        insnList2.add(new IntInsnNode(BIPUSH, 12));
                        insnList2.add(new JumpInsnNode(IF_ICMPEQ, l40));

                        m.instructions.insertBefore(abstractInsnNode, insnList2);
                        break;
                    }
                }
            }
        }

        ClassWriter cw = new ClassWriter(ClassWriter.COMPUTE_FRAMES + ClassWriter.COMPUTE_MAXS);

        classNode.accept(cw);
        LogManager.getLogger("LetsPlay-OreKrasher").log(Level.INFO, "Transformed BlockMachine!");

        return cw.toByteArray();
    }

    private byte[] transformTypes(byte[] before){
        ClassNode classNode = new ClassNode();
        ClassReader reader = new ClassReader(before);

        reader.accept(classNode, 0);

        classNode.fields.add(12, new FieldNode(ACC_PUBLIC + ACC_STATIC + ACC_FINAL + ACC_ENUM, "KRASHER", "Lcofh/thermalexpansion/block/machine/BlockMachine$Types;", null, null));

        for (MethodNode m : classNode.methods) {
            if(!m.name.equals("<clinit>"))
                continue;

            for(int i = 0; i < m.instructions.size(); i++){
                AbstractInsnNode abstractInsnNode = m.instructions.get(i);

                if(abstractInsnNode instanceof IntInsnNode){
                    IntInsnNode intInsnNode = (IntInsnNode) abstractInsnNode;
                    if(intInsnNode.getOpcode() == BIPUSH && intInsnNode.operand == 12){
                        m.instructions.insertBefore(intInsnNode, new IntInsnNode(BIPUSH, 13));
                        m.instructions.remove(intInsnNode);
                        break;
                    }
                }
            }

            for(int i = 0; i < m.instructions.size(); i++){
                AbstractInsnNode abstractInsnNode = m.instructions.get(i);

                if(abstractInsnNode instanceof FieldInsnNode){
                    FieldInsnNode fieldInsnNode = (FieldInsnNode) abstractInsnNode;

                    if(fieldInsnNode.getOpcode() == PUTSTATIC && fieldInsnNode.name.equalsIgnoreCase("INSOLATOR")){
                        InsnList insnList = new InsnList();

                        insnList.add(new TypeInsnNode(NEW, "cofh/thermalexpansion/block/machine/BlockMachine$Types"));
                        insnList.add(new InsnNode(DUP));
                        insnList.add(new LdcInsnNode("KRASHER"));
                        insnList.add(new IntInsnNode(BIPUSH, 12));
                        insnList.add(new MethodInsnNode(INVOKESPECIAL, "cofh/thermalexpansion/block/machine/BlockMachine$Types", "<init>", "(Ljava/lang/String;I)V", false));
                        insnList.add(new FieldInsnNode(PUTSTATIC, "cofh/thermalexpansion/block/machine/BlockMachine$Types", "KRASHER", "Lcofh/thermalexpansion/block/machine/BlockMachine$Types;"));
                        m.instructions.insert(abstractInsnNode, insnList);
                        break;
                    }
                }
            }

            for(int i = m.instructions.size() - 1; i >= 0; i--){
                AbstractInsnNode abstractInsnNode = m.instructions.get(i);

                if(abstractInsnNode.getOpcode() == AASTORE){
                    InsnList insnList = new InsnList();
                    insnList.add(new InsnNode(DUP));
                    insnList.add(new IntInsnNode(BIPUSH, 12));
                    insnList.add(new FieldInsnNode(GETSTATIC, "cofh/thermalexpansion/block/machine/BlockMachine$Types", "KRASHER", "Lcofh/thermalexpansion/block/machine/BlockMachine$Types;"));
                    insnList.add(new InsnNode(AASTORE));

                    m.instructions.insert(abstractInsnNode, insnList);
                    break;
                }
            }
        }

        ClassWriter cw = new ClassWriter(3);

        classNode.accept(cw);
        LogManager.getLogger("LetsPlay-OreKrasher").log(Level.INFO, "Transformed BlockMachine$Types!");

        return cw.toByteArray();
    }

}

package powercrystals.netherores.plugins.nei;

import codechicken.lib.gui.GuiDraw;
import codechicken.nei.NEIServerUtils;
import codechicken.nei.PositionedStack;
import cofh.thermalexpansion.block.TEBlocks;
import cofh.thermalexpansion.block.simple.BlockRockwool;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import powercrystals.netherores.gui.client.machine.GuiKrasher;
import powercrystals.netherores.util.crafting.KrasherManager;

public class RecipeHandlerKrasher extends RecipeHandlerBase {

    public static RecipeHandlerKrasher instance = new RecipeHandlerKrasher();

    public RecipeHandlerKrasher() {
        this.maxEnergy = 24000;
    }

    public void initialize() {
        this.trCoords = new int[]{74, 23, 24, 18};
        this.recipeName = "krasher";
        this.containerClass = GuiKrasher.class;
    }

    public void drawBackgroundExtras(int var1) {
        GuiDraw.drawTexturedModalRect(50, 17, 132, 96, 18, 18);
        GuiDraw.drawTexturedModalRect(106, 22, 150, 96, 26, 26);
        GuiDraw.drawTexturedModalRect(51, 36, 224, 32, 16, 16);
        this.drawProgressBar(51, 36, 240, 32, 16, 16, 100, 7);
        GuiDraw.drawTexturedModalRect(74, 24, 176, 16, 24, 16);
        this.drawProgressBar(74, 24, 200, 16, 24, 16, 20, 0);
    }

    public void drawExtras(int var1) {
        this.drawEnergy(var1);
        int var2 = ((RecipeHandlerBase.NEIRecipeBase)this.arecipes.get(var1)).energy;
        if (var2 < 1000) {
            GuiDraw.drawString(var2 + "RF", 46, 54, 9671571, false);
        } else {
            GuiDraw.drawString(var2 + "RF", 40, 54, 9671571, false);
        }

    }

    public void loadCraftingRecipes(String var1, Object... var2) {
        if (var1.equals(this.getOverlayIdentifier())) {
            KrasherManager.RecipeKrasher[] var3 = KrasherManager.getRecipeList();
            KrasherManager.RecipeKrasher[] var4 = var3;
            int var5 = var3.length;

            for(int var6 = 0; var6 < var5; ++var6) {
                KrasherManager.RecipeKrasher var7 = var4[var6];
                this.arecipes.add(new RecipeHandlerKrasher.NEIRecipeKrasher(var7));
            }
        } else {
            super.loadCraftingRecipes(var1, var2);
        }

    }

    public void loadCraftingRecipes(ItemStack var1) {
        KrasherManager.RecipeKrasher[] var2 = KrasherManager.getRecipeList();
        KrasherManager.RecipeKrasher[] var3 = var2;
        int var4 = var2.length;

        int var5;
        KrasherManager.RecipeKrasher var6;
        for(var5 = 0; var5 < var4; ++var5) {
            var6 = var3[var5];
            if (NEIServerUtils.areStacksSameType(var6.getOutput(), var1)) {
                this.arecipes.add(new RecipeHandlerKrasher.NEIRecipeKrasher(var6));
            }
        }

        if (var1.getItem() == Item.getItemFromBlock(TEBlocks.blockRockwool) && var1.getItemDamage() != BlockRockwool.DEFAULT_META) {
            var1 = new ItemStack(TEBlocks.blockRockwool, 1, BlockRockwool.DEFAULT_META);
            var3 = var2;
            var4 = var2.length;

            for(var5 = 0; var5 < var4; ++var5) {
                var6 = var3[var5];
                if (NEIServerUtils.areStacksSameType(var6.getOutput(), var1)) {
                    this.arecipes.add(new RecipeHandlerKrasher.NEIRecipeKrasher(var6));
                }
            }
        }

    }

    public void loadUsageRecipes(String var1, Object... var2) {
        if (var1.equals("fuel") && this.getClass() == RecipeHandlerKrasher.class) {
            this.loadCraftingRecipes(this.getOverlayIdentifier());
        } else {
            super.loadUsageRecipes(var1, var2);
        }

    }

    public void loadUsageRecipes(ItemStack var1) {
        KrasherManager.RecipeKrasher[] var2 = KrasherManager.getRecipeList();
        KrasherManager.RecipeKrasher[] var3 = var2;
        int var4 = var2.length;

        for(int var5 = 0; var5 < var4; ++var5) {
            KrasherManager.RecipeKrasher var6 = var3[var5];
            if (NEIServerUtils.areStacksSameType(var6.getInput(), var1)) {
                this.arecipes.add(new RecipeHandlerKrasher.NEIRecipeKrasher(var6));
            }
        }

    }

    class NEIRecipeKrasher extends RecipeHandlerBase.NEIRecipeBase {
        public NEIRecipeKrasher(KrasherManager.RecipeKrasher var2) {
            this.input = new PositionedStack(var2.getInput(), 51, 18);
            this.output = new PositionedStack(var2.getOutput(), 111, 27);
            this.energy = var2.getEnergy();
            this.setOres();
        }
    }

}

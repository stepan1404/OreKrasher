package powercrystals.netherores.plugins.nei;

import codechicken.lib.gui.GuiDraw;
import codechicken.nei.NEIClientConfig;
import codechicken.nei.PositionedStack;
import codechicken.nei.recipe.GuiCraftingRecipe;
import codechicken.nei.recipe.GuiRecipe;
import codechicken.nei.recipe.GuiUsageRecipe;
import codechicken.nei.recipe.TemplateRecipeHandler;
import cofh.lib.inventory.ComparableItemStackSafe;
import cofh.lib.render.RenderHelper;
import cofh.lib.util.helpers.ItemHelper;
import cofh.lib.util.helpers.StringHelper;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.item.ItemStack;
import net.minecraft.util.IIcon;
import net.minecraftforge.fluids.FluidStack;
import net.minecraftforge.oredict.OreDictionary;
import org.lwjgl.opengl.GL11;

import java.awt.*;
import java.util.ArrayList;

public abstract class RecipeHandlerBase extends TemplateRecipeHandler {
    Class<? extends GuiContainer> containerClass;
    String recipeName;
    static final String TEXTURE = "thermalexpansion:textures/gui/NEIHandler.png";
    int[] trCoords = new int[4];
    int maxEnergy = 24000;
    int scaleEnergy = 42;
    int maxFluid = 10000;
    int scaleFluid = 60;
    int[] energyAmount = new int[2];
    int[] fluidAmount = new int[2];
    int[] lastCycle = new int[2];
    int[] arecipe = new int[]{-1, -1};

    public RecipeHandlerBase() {
    }

    public void loadTransferRects() {
        this.initialize();
        this.transferRects.add(new RecipeTransferRect(new Rectangle(this.trCoords[0], this.trCoords[1], this.trCoords[2], this.trCoords[3]), this.getOverlayIdentifier(), new Object[0]));
    }

    public Class<? extends GuiContainer> getGuiClass() {
        return this.containerClass;
    }

    public String getRecipeName() {
        return StringHelper.localize("tile.thermalexpansion.machine." + this.recipeName + ".name");
    }

    public String getGuiTexture() {
        return "thermalexpansion:textures/gui/NEIHandler.png";
    }

    public String getOverlayIdentifier() {
        return "thermalexpansion." + this.recipeName;
    }

    public abstract void initialize();

    public void drawBackground(int var1) {
        GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
        GuiDraw.changeTexture(this.getGuiTexture());
        GuiDraw.drawTexturedModalRect(0, 0, 5, 11, 166, 65);
        this.drawBackgroundExtras(var1);
    }

    public void drawBackgroundExtras(int var1) {
    }

    public boolean keyTyped(GuiRecipe var1, char var2, int var3, int var4) {
        if (var3 == NEIClientConfig.getKeyBinding("gui.recipe")) {
            if (this.transferFluidTank(var1, var4, false)) {
                return true;
            }
        } else if (var3 == NEIClientConfig.getKeyBinding("gui.usage") && this.transferFluidTank(var1, var4, true)) {
            return true;
        }

        return super.keyTyped(var1, var2, var3, var4);
    }

    public boolean mouseClicked(GuiRecipe var1, int var2, int var3) {
        if (var2 == 0) {
            if (this.transferFluidTank(var1, var3, false)) {
                return true;
            }
        } else if (var2 == 1 && this.transferFluidTank(var1, var3, true)) {
            return true;
        }

        return super.mouseClicked(var1, var2, var3);
    }

    protected boolean transferFluidTank(GuiRecipe var1, int var2, boolean var3) {
        short var4 = 153;
        short var5 = 169;
        byte var6 = 19;
        byte var7 = 79;
        byte var8 = 65;
        Point var9 = GuiDraw.getMousePosition();
        FluidStack var10 = null;
        if (var9.x >= var4 + var1.guiLeft && var9.x < var5 + var1.guiLeft && var9.y >= var6 + var1.guiTop && var9.y < var7 + var1.guiTop && this.arecipe[0] == var2) {
            var10 = ((RecipeHandlerBase.NEIRecipeBase)this.arecipes.get(var2)).fluid;
        } else if (var9.x >= var4 + var1.guiLeft && var9.x < var5 + var1.guiLeft && var9.y >= var6 + var1.guiTop + var8 && var9.y < var7 + var1.guiTop + var8 && this.arecipe[1] == var2) {
            var10 = ((RecipeHandlerBase.NEIRecipeBase)this.arecipes.get(var2)).fluid;
        }

        if (var10 != null && var10.amount > 0) {
            if (var3) {
                if (GuiUsageRecipe.openRecipeGui("liquid", new Object[]{var10})) {
                    return true;
                }
            } else if (GuiCraftingRecipe.openRecipeGui("liquid", new Object[]{var10})) {
                return true;
            }
        }

        return false;
    }

    private void resetCounters() {
        this.arecipe[0] = -1;
        this.arecipe[1] = -1;
        this.energyAmount[0] = 0;
        this.energyAmount[1] = 0;
        this.fluidAmount[0] = 0;
        this.fluidAmount[1] = 0;
        this.lastCycle[0] = 0;
        this.lastCycle[1] = 0;
    }

    public void drawEnergy(int var1) {
        byte var2 = 0;
        if (this.arecipe[0] == -1) {
            this.arecipe[0] = var1;
        } else if (this.arecipe[1] == -1 && this.arecipe[0] != var1) {
            this.arecipe[1] = var1;
        }

        if (this.arecipe[0] != var1 && this.arecipe[1] != var1) {
            this.resetCounters();
            this.drawEnergy(var1);
        } else {
            if (this.arecipe[1] == var1) {
                var2 = 1;
            }

            GuiDraw.drawTexturedModalRect(4, 2, 0, 96, 16, this.scaleEnergy);
            int var3 = this.getScaledEnergy(this.energyAmount[var2]);
            GuiDraw.drawTexturedModalRect(4, 2 + var3, 16, 96 + var3, 16, this.scaleEnergy - var3);
            if (this.cycleticks % 20 == 0 && this.cycleticks != this.lastCycle[var2]) {
                if (this.energyAmount[var2] == this.maxEnergy) {
                    this.energyAmount[var2] = 0;
                }

                this.energyAmount[var2] += ((RecipeHandlerBase.NEIRecipeBase)this.arecipes.get(var1)).energy;
                if (this.energyAmount[var2] > this.maxEnergy) {
                    this.energyAmount[var2] = this.maxEnergy;
                }

                this.lastCycle[var2] = this.cycleticks;
            }

        }
    }

    public void drawFluid(int var1, boolean var2) {
        byte var3 = 0;
        if (this.arecipe[0] == -1) {
            this.arecipe[0] = var1;
        } else if (this.arecipe[1] == -1 && this.arecipe[0] != var1) {
            this.arecipe[1] = var1;
        }

        if (this.arecipe[0] != var1 && this.arecipe[1] != var1) {
            this.resetCounters();
            this.drawFluid(var1, var2);
        } else {
            if (this.arecipe[1] == var1) {
                var3 = 1;
            }

            GuiDraw.drawTexturedModalRect(147, 2, 32, 96, 18, this.scaleFluid + 2);
            int var4 = this.getScaledFluid(this.fluidAmount[var3]);
            if (var2) {
                this.drawFluidRect(148, 3 + this.scaleFluid - var4, ((RecipeHandlerBase.NEIRecipeBase)this.arecipes.get(var1)).fluid, 16, var4);
            } else {
                this.drawFluidRect(148, 3 + var4, ((RecipeHandlerBase.NEIRecipeBase)this.arecipes.get(var1)).fluid, 16, this.scaleFluid - var4);
            }

            if (this.cycleticks % 20 == 0 && this.cycleticks != this.lastCycle[var3]) {
                if (this.fluidAmount[var3] == this.maxFluid) {
                    this.fluidAmount[var3] = 0;
                }

                this.fluidAmount[var3] += ((RecipeHandlerBase.NEIRecipeBase)this.arecipes.get(var1)).fluid.amount;
                if (this.fluidAmount[var3] > this.maxFluid) {
                    this.fluidAmount[var3] = this.maxFluid;
                }
            }

            GuiDraw.drawTexturedModalRect(148, 2, 80, 96, 18, this.scaleFluid + 2);
        }
    }

    public int getScaledEnergy(int var1) {
        return var1 * this.scaleEnergy / this.maxEnergy;
    }

    public int getScaledFluid(int var1) {
        return var1 * this.scaleFluid / this.maxFluid;
    }

    protected void drawFluidRect(int var1, int var2, FluidStack var3, int var4, int var5) {
        if (var5 > this.scaleFluid) {
            var5 = this.scaleFluid;
        }

        boolean var6 = false;
        boolean var7 = false;
        RenderHelper.setBlockTextureSheet();
        RenderHelper.setColor3ub(var3.getFluid().getColor(var3));

        for(int var8 = 0; var8 < var4; var8 += 16) {
            for(int var9 = 0; var9 < var5; var9 += 16) {
                int var11 = Math.min(var4 - var8, 16);
                int var10 = Math.min(var5 - var9, 16);
                drawScaledTexturedModelRectFromIcon(var1 + var8, var2 + var9, var3.getFluid().getIcon(), var11, var10);
            }
        }

        GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
        GuiDraw.changeTexture(this.getGuiTexture());
    }

    public static void drawScaledTexturedModelRectFromIcon(int var0, int var1, IIcon var2, int var3, int var4) {
        if (var2 != null) {
            double var5 = (double)var2.getMinU();
            double var7 = (double)var2.getMaxU();
            double var9 = (double)var2.getMinV();
            double var11 = (double)var2.getMaxV();
            Tessellator var13 = Tessellator.instance;
            var13.startDrawingQuads();
            var13.addVertexWithUV((double)(var0 + 0), (double)(var1 + var4), (double)GuiDraw.gui.getZLevel(), var5, var9 + (var11 - var9) * (double)var4 / 16.0D);
            var13.addVertexWithUV((double)(var0 + var3), (double)(var1 + var4), (double)GuiDraw.gui.getZLevel(), var5 + (var7 - var5) * (double)var3 / 16.0D, var9 + (var11 - var9) * (double)var4 / 16.0D);
            var13.addVertexWithUV((double)(var0 + var3), (double)(var1 + 0), (double)GuiDraw.gui.getZLevel(), var5 + (var7 - var5) * (double)var3 / 16.0D, var9);
            var13.addVertexWithUV((double)(var0 + 0), (double)(var1 + 0), (double)GuiDraw.gui.getZLevel(), var5, var9);
            var13.draw();
        }
    }

    abstract class NEIRecipeBase extends CachedRecipe {
        PositionedStack input = null;
        PositionedStack secondaryInput = null;
        PositionedStack output = null;
        PositionedStack secondaryOutput = null;
        String inputOreName = "Unknown";
        ArrayList<ItemStack> inputList = null;
        String secondaryInputOreName = "Unknown";
        ArrayList<ItemStack> secondaryList = null;
        boolean cycleInput = true;
        boolean cycleSecondary = true;
        int inputOrePosition = 0;
        int secondaryOrePosition = 0;
        FluidStack fluid = null;
        int secondaryOutputChance = 0;
        int energy = 0;

        NEIRecipeBase() {
            super();
        }

        protected void setOres() {
            if (this.input != null) {
                this.inputOreName = ItemHelper.getOreName(this.input.item);
                if (!this.inputOreName.equals("Unknown")) {
                    this.inputList = OreDictionary.getOres(this.inputOreName);
                }

                this.cycleInput &= ComparableItemStackSafe.getOreID(this.inputOreName) != -1;
            }

            if (this.secondaryInput != null) {
                this.secondaryInputOreName = ItemHelper.getOreName(this.secondaryInput.item);
                if (!this.secondaryInputOreName.equals("Unknown")) {
                    this.secondaryList = OreDictionary.getOres(this.secondaryInputOreName);
                }

                this.cycleSecondary &= ComparableItemStackSafe.getOreID(this.secondaryInputOreName) != -1;
            }

        }

        protected void incrementPrimary() {
            if (this.inputList != null && this.inputList.size() > 0) {
                if (this.cycleInput && !this.inputOreName.equals("Unknown")) {
                    ++this.inputOrePosition;
                    this.inputOrePosition %= this.inputList.size();
                    int var1 = this.input.item.stackSize;
                    this.input.item = (ItemStack)this.inputList.get(this.inputOrePosition);
                    this.input.item.stackSize = var1;
                    if (((ItemStack)this.inputList.get(this.inputOrePosition)).getItemDamage() != 32767) {
                        this.input.item.setItemDamage(((ItemStack)this.inputList.get(this.inputOrePosition)).getItemDamage());
                    }
                }

            }
        }

        protected void incrementSecondary() {
            if (this.secondaryList != null && this.secondaryList.size() > 0) {
                if (this.cycleSecondary && !this.secondaryInputOreName.equals("Unknown")) {
                    ++this.secondaryOrePosition;
                    this.secondaryOrePosition %= this.secondaryList.size();
                    int var1 = this.secondaryInput.item.stackSize;
                    this.secondaryInput.item = (ItemStack)this.secondaryList.get(this.secondaryOrePosition);
                    this.secondaryInput.item.stackSize = var1;
                    if (((ItemStack)this.secondaryList.get(this.secondaryOrePosition)).getItemDamage() != 32767) {
                        this.secondaryInput.item.setItemDamage(((ItemStack)this.secondaryList.get(this.secondaryOrePosition)).getItemDamage());
                    }
                }

            }
        }

        public PositionedStack getIngredient() {
            if (RecipeHandlerBase.this.cycleticks % 20 == 0) {
                this.incrementPrimary();
            }

            return this.input;
        }

        public PositionedStack getResult() {
            return this.output;
        }

        public ArrayList<PositionedStack> getOtherStacks() {
            ArrayList var1 = new ArrayList();
            if (this.secondaryOutput != null) {
                var1.add(this.secondaryOutput);
            }

            if (this.secondaryInput != null) {
                if (RecipeHandlerBase.this.cycleticks % 20 == 0) {
                    this.incrementSecondary();
                }

                var1.add(this.secondaryInput);
            }

            return var1;
        }
    }
}

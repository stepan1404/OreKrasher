package powercrystals.netherores.gui.client.machine;

import cofh.lib.gui.element.ElementBase;
import cofh.lib.gui.element.ElementDualScaled;
import cofh.lib.gui.element.ElementEnergyStored;
import cofh.thermalexpansion.gui.client.GuiAugmentableBase;
import cofh.thermalexpansion.gui.element.ElementSlotOverlay;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ResourceLocation;
import powercrystals.netherores.gui.container.machine.ContainerKrasher;

public class GuiKrasher extends GuiAugmentableBase {

    public static final ResourceLocation TEXTURE = new ResourceLocation("thermalexpansion:textures/gui/machine/Furnace.png");
    ElementBase slotInput;
    ElementBase slotOutput;
    ElementDualScaled progress;
    ElementDualScaled speed;

    public GuiKrasher(InventoryPlayer var1, TileEntity var2) {
        super(new ContainerKrasher(var1, var2), var2, var1.player, TEXTURE);
        this.generateInfo("tab.thermalexpansion.machine.krasher", 3);
    }

    public void initGui() {
        super.initGui();
        this.slotInput = this.addElement((new ElementSlotOverlay(this, 56, 26)).setSlotInfo(0, 0, 2));
        this.slotOutput = this.addElement((new ElementSlotOverlay(this, 112, 31)).setSlotInfo(3, 1, 2));
        this.addElement(new ElementEnergyStored(this, 8, 8, this.myTile.getEnergyStorage()));
        this.progress = (ElementDualScaled)this.addElement((new ElementDualScaled(this, 79, 34)).setMode(1).setSize(24, 16).setTexture("cofh:textures/gui/elements/Progress_Arrow_Right.png", 64, 16));
        this.speed = (ElementDualScaled)this.addElement((new ElementDualScaled(this, 56, 44)).setSize(16, 16).setTexture("cofh:textures/gui/elements/Scale_Flame.png", 32, 16));
    }

    protected void updateElementInformation() {
        super.updateElementInformation();
        this.slotInput.setVisible(this.myTile.hasSide(1));
        this.slotOutput.setVisible(this.myTile.hasSide(2));
        this.progress.setQuantity(this.myTile.getScaledProgress(24));
        this.speed.setQuantity(this.myTile.getScaledSpeed(16));
    }

}

package powercrystals.netherores.gui.container.machine;

import cofh.lib.gui.slot.ISlotValidator;
import cofh.lib.gui.slot.SlotEnergy;
import cofh.lib.gui.slot.SlotValidated;
import cofh.thermalexpansion.gui.container.ContainerTEBase;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.inventory.SlotFurnace;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import powercrystals.netherores.block.machine.TileKrasher;
import powercrystals.netherores.util.crafting.KrasherManager;

public class ContainerKrasher extends ContainerTEBase implements ISlotValidator {

    TileKrasher myTile;

    public ContainerKrasher(InventoryPlayer var1, TileEntity var2) {
        super(var1, var2);
        this.myTile = (TileKrasher) var2;
        this.addSlotToContainer(new SlotValidated(this, this.myTile, 0, 56, 26));
        this.addSlotToContainer(new SlotFurnace(var1.player, this.myTile, 1, 116, 35));
        this.addSlotToContainer(new SlotEnergy(this.myTile, this.myTile.getChargeSlot(), 8, 53));
    }

    public boolean isItemValid(ItemStack var1) {
        return KrasherManager.recipeExists(var1);
    }

}
